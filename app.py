from flask import Flask, render_template, request, redirect, flash
from flask_sqlalchemy import SQLAlchemy
import sys
import requests
import datetime as dt


SESSION_TYPE = 'memcache'

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///weather.db'
db = SQLAlchemy(app)


class City(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)


with app.app_context():
    db.create_all()

    response_db = City.query.all()


@app.route('/delete/<int:city_id>', methods=['POST'])
def delete(city_id):
    city = City.query.filter_by(id=city_id).first()
    if city is None:
        flash("The city doesn't exist!")
        return redirect('/')
    db.session.delete(city)
    db.session.commit()
    return redirect('/')


@app.route('/', methods=['GET'])
def get():
    if City.query.all() is not None and request.method == 'GET':
        list_of_cities = [i.name for i in City.query.all()]

        big_dict = []
        for city in list_of_cities:
            url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=1b5e2bcc16da3ef25055c8796e8a8563'.format(city)
            response_api = requests.get(url).json()

            current_temperature_celsius = round(response_api.get('main', {}).get('temp') - 273.15, 2)
            weather = response_api.get('weather')[0].get('main')
            time = response_api.get('timezone') / 3600
            now_moscow = dt.datetime.strptime(dt.datetime.today().strftime('%H:%M'), '%H:%M')
            there_time = now_moscow + dt.timedelta(hours=-3) + dt.timedelta(hours=int(time // 1), minutes=int(time % 1 * 60))
            city_id = City.query.filter_by(name=city).first().id
            h = there_time.hour if there_time.hour >= 10 else str(0) + str(there_time.hour)
            m = there_time.minute if there_time.minute >= 10 else str(0) + str(there_time.minute)
            small_dict = [{'1': city, '2': weather, '3': int(round(current_temperature_celsius, 0)), '4': city_id, '5': f'{h}:{m}', '6': there_time.hour}]
            big_dict += small_dict

        return render_template('index.html', weather={'1': big_dict})
    else:
        return render_template('index.html')


@app.route('/add', methods=['POST'])
def add_city():
    if request.method == 'POST':
        name_of_city = request.form.get('city_name')
        url = 'http://api.openweathermap.org/data/2.5/weather?q={}&APPID=1b5e2bcc16da3ef25055c8796e8a8563'.format(name_of_city)
        response_api = requests.get(url).json().get('cod')
        if response_api == '404':
            flash("The city doesn't exist!")
            return redirect('/')
        elif City.query.filter_by(name=name_of_city).first() is not None:
            flash("The city has already been added to the list!")
            return redirect('/')
        else:
            db.session.add(City(name=name_of_city))
            db.session.commit()

    return redirect('/')


app.add_url_rule('/delete/<int:city_id>', 'Delete', delete)
app.add_url_rule('/add', 'Post', add_city)


if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    if len(sys.argv) > 1:
        arg_host, arg_port = sys.argv[1].split(':')
        app.run(host=arg_host, port=arg_port)
    else:
        app.run()
